#!/usr/bin/perl

use Cwd;
use Cwd 'abs_path';
use File::Basename;
#use File::Which;
use POSIX qw/strftime/;
use Sys::Hostname;

my $meta;
if(1) {
  $cmdline = "Unknown";
  eval ('$cmdline=`cat /proc/'.$$.'/cmdline`') && $cmdline =~ s/\x0/<NULL>/g;
  
  $cwd = getcwd;  # must be a global variable
  $progname0 = basename($0);
  $progdir0 = dirname($0);
  $hgid = "Unknown version"; # must be a global variable
  $curtime = strftime("%Y-%m-%d %H:%M", localtime);
  
  # Get original program name if the script is a symbolic link
  my $true0 = $0;
  while (-l $true0) { $true0 = readlink($true0); }
  $true0 = abs_path($true0);
  $truedir  = dirname($true0); # must be a global variable
  $truename = basename($true0); # must be a global variable
  my $tmp=$truedir;
  while( !-e "$tmp/common_header_hg.pl" && length($tmp) > 1) {
    $tmp = dirname($tmp);
  }
  # The following line tries to commit the source code.  
  # There's no warning if common_header_hg.pl cannot be found.
  # common_header_hg.pl is expected to be available on a development machine, but 
  # not on a production machine.  
  eval "require '$tmp/common_header_hg.pl'" ;
  #print "#$true0; $hgid\n"; die;

  $meta="#CMD=$cmdline; HOST=".hostname."; TIME=$curtime; CWD=$cwd; PROG=$true0; VERSION=$hgid";
}

#print $meta, "\n";

# usage
my $usage = "SUMMARY:
  Fix population names (subsitute <D0> and <D1> with '-') in FlowJo XML workspace file.

USAGE:
  $progname0 [-stdout] workspace.xml [...]

";

if(scalar @ARGV == 0) {
  if($hgid =~ /^Unknown/) {
  } else {
    my $short_ver = $hgid;
    $short_ver =~ s/\).*$/\)/g;
    print "VERSION:\n  $short_ver\n";
  }
  print $usage;
  exit -1;
}

my $use_stdout = 0;

LOOP:
foreach my $arg (@ARGV) {
  if($arg =~ /^-stdout/) {
    $use_stdout++;
    next LOOP;
  }
  my $file = $arg;
  chomp($file);
  if(!-e $file) {
    $cwd = getcwd;
    print STDERR "Cwd: ", $cwd, "\n";
    print STDERR "File not found: '$file'\n";
    next LOOP;
  }
  my $d = dirname($file);
  my $f = basename($file, (".xml", ".XML"));
  my $output;
  my $version_output;
  if($use_stdout) {
    $output = \*STDOUT;
  } else {
    $output_file = "$d/$f.fixed.xml";
    $output_file =~ s/\s/./g;
    $version_output_file = $output_file . ".runinfo";
    open($output, ">$output_file");
    open($version_output, ">$version_output_file");
    print $version_output $meta;
    close($version_output);
  }
  #print "$file; $d; $f\n";
  open(F, "<$file");
LOOP:
  while(<F>) {
    chomp;
    s/[\n\r]/\n/g;
    s/[\xd0\xd1]/-/g;
    s/[^\n]*OverlayGraphs[^\n]*//g;
    s/[^\n]*graphList[^\n]*//g;
    #s/([^\n]*<(GroupNode|Population|SampleNode)[^\n]*)nodeName([^\n]*)/$1name$3/g;
    #s/([^\n]*)<GraphInformation([^\n]*)/$1<Graph$2/g;
    #s/([^\n]*)<\/GraphInformation([^\n]*)/$1<\/Graph$2/g;
    #s/([^\n]*)<Samples([^\n]*)/$1<SampleList$2/g;
    #s/([^\n]*)<\/Samples([^\n]*)/$1<\/SampleList$2/g;
    print $output $_, "\n";
  }
  close(F);
}
